﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assign3_mdi
{
    /// <summary>
    /// class holds information about people Identified in window. well have to create seven of these.
    /// different professors each can be added via public Identified_Person(string in_name)
    /// </summary>
    public class Identified_Person
    {
        public string name;
        public string office_number;
        public string description;
        public string picture_string;
        public ImageSource picture;

        public Identified_Person(string in_name, string in_office_number, string in_description, string in_picture_string) //Image in_face_picture)
        {
            this.name = in_name;
            this.office_number = in_office_number;
            this.description = in_description;
            this.picture_string = in_picture_string;
            this.picture = new BitmapImage(new Uri("pack://application:,,,/Resources/"+in_picture_string));
        }
    }
}