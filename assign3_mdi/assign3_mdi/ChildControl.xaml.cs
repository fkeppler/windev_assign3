﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assign3_mdi
{
    /// <summary>
    /// Interaction logic for ChildControl.xaml
    /// </summary>
    public partial class ChildControl : UserControl
    {
        public ChildControl()
        {
            InitializeComponent();
        }

        public ChildControl(Identified_Person person)
        {
            name_box.Text = person.name;
            room_box.Text = person.office_number;
            description_box.Text = person.description;
            portrait_box.Source = person.picture;
        }


        private void portrait_box_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
    }
}
