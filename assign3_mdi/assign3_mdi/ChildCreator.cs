﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WPF.MDI;

namespace assign3_mdi
{
    static class ChildCreator
    {
        /// <summary>
        /// this function creates a new mdichild window when given an identified person object.
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>

        public static MdiChild create_new_child(Identified_Person person)
        {
            ChildControl background = new ChildControl(person);
            MdiChild result = new MdiChild()
            {
                Title = person.name,
                Content = background
            };
            return result;
        }
        /// <summary>
        /// adds child as a child window of holder if a child with the same title does not alreayd exist.
        /// </summary>
        /// <param name="holder"></param>
        /// <param name="child"></param>
        public static void add_child(MdiContainer holder, MdiChild child)
        {
            if (!holder.Children.Any(p => p.Title == child.Title))
            {
                holder.Children.Add(child);
            }
        }
    }
}
