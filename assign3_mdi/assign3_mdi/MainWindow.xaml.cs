﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF.MDI;


namespace assign3_mdi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        //this is for the file button
        {

        }

        private void exit_Click(object sender, RoutedEventArgs e)
            //this one is for the exit button
        {
            this.Close();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            //TODO: add other names here (and other relevant information)
            //TODO: format output
            MessageBox.Show("Authors: FORREST KEPPLER");
        }

#region file-->open click event handlers
        private void add_Granier(object sender, RoutedEventArgs e)
        {
            Identified_Person Granier = new Identified_Person("Martin Granier", "00", "Director, Internet Studies Center", "martin_granier.jpg");
            ChildCreator.add_child(WindowContainer, ChildCreator.create_new_child(Granier));
        }

        private void add_Reedy(object sender, RoutedEventArgs e)
        {
        }

        private void add_Johnson(object sender, RoutedEventArgs e)
        {
        }

        private void add_Fugier(object sender, RoutedEventArgs e)
        {
        }

        private void add_Hall(object sender, RoutedEventArgs e)
        {
        }

        private void add_Fizzano(object sender, RoutedEventArgs e)
        {
        }

        private void add_Nordwall(object sender, RoutedEventArgs e)
        {

        }
#endregion

#region window click handlers
        private void cascade_click(object sender, RoutedEventArgs e)
        {
            WindowContainer.MdiLayout = MdiLayout.Cascade;
        }

        private void horizontal_tile_click(object sender, RoutedEventArgs e)
        {
            WindowContainer.MdiLayout = MdiLayout.TileHorizontal;
        }

        private void vertical_tile_click(object sender, RoutedEventArgs e)
        {
             WindowContainer.MdiLayout = MdiLayout.TileVertical;
        }
#endregion
    }
}
